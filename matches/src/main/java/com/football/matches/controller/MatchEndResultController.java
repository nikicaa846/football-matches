package com.football.matches.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.football.matches.model.MatchEndResultEvent;
import com.football.matches.service.MatchEndResultService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/results")
public class MatchEndResultController {

	@Autowired
	private MatchEndResultService service;
	
	@GetMapping
	public List<MatchEndResultEvent> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping(consumes = MediaType.ALL_VALUE)
	public MatchEndResultEvent addMatchEndResult(@RequestParam Map<Object, Object> data) {
		return this.service.addMatchEndResult(data);
	}
}

