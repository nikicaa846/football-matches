package com.football.matches.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.football.matches.model.MatchEndResultEvent;


@Repository
public interface MatchEndResultRepository extends JpaRepository<MatchEndResultEvent, UUID> {

}
