package com.football.matches.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "matchEndResult")
public class MatchEndResultEvent{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID matchId;
	private String matchName;
	private String endResult;
	
}