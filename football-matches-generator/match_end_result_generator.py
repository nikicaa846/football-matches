from match_event.match_end_result_event import matchEndResultEvent
import requests
from threading import Timer

def match():
    m = matchEndResultEvent()
    print(m.matchId, m.matchName, m.endResult)
    r = requests.post("http://localhost:9090/api/v1/results",
        data={"matchId": m.matchId, "matchName": m.matchName, "endResult":m.endResult}
    )

while True:
    t = Timer(1.0, match )
    t.run()
