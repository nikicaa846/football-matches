import uuid
import random

def getMatchName():
    matches = ["Manchester United", "Zrinjski", "Partizan", "Crvena zvijezda",
               "Liverpool", "Real Madrid", "Barselona", "Chelsea", " AC Milan",
               "Bayern Munich"]
    x = random.randrange(10)
    y = random.randrange(10)
    while x == y:
        y = random.randrange(10)
    return matches[x] + ":" + matches[y]

class matchEndResultEvent:
    def __init__(self):
        self.matchId = uuid.uuid4()
        self.matchName = getMatchName()
        self.endResult = str(random.randrange(7)) + \
            ":" + str(random.randrange(7))
